const express = require("express");
const app = express();
const PORT = 3000;

// GET endpoint - http://127.0.0.1:3000/
app.get('/', (req, res) => {
    res.send("Welcome!");
});

/**
 * 
 * @param {number} multiplicant 
 * @param {number} multiplier 
 * @returns {number} product
 */
const multiply = (multiplicant, multiplier) => {
    const product = multiplicant * multiplier;
    return product;
}

app.get("/multiply", (req, res) => {
    try {
        const multiplicant = req.query.a;
        const multiplier = req.query.b;
        if (isNaN(multiplicant)) throw new Error("Invalid value.");
        if (isNaN(multiplier)) throw new Error("Invalid value.");
        console.log({multiplicant, multiplier});
        const product = multiply(multiplicant,multiplier);
        res.send(product.toString());
    }catch (err) {
        console.error(err);
        res.send("Couldn't calculate the product. Try again.");
    }
});

app.listen(PORT, () => console.log(
    `Listening at http://127.0.0.1:${PORT}`
    ));