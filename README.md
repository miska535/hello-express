# hello-express

This project contains REST API with following features:

- Welcome endpoint
- Multiply endpoint

## Getting started

First clone the project
```
clone git@gitlab.com:miska535/hello-express.git
cd hello-express
```

Install the dependencies 

```
npm i
```

Start the REST API service:

`node app.js`

Or in the latest NodeJS version (>=20):

`node --watch app.js`

## Browser compatibility
| Chrome | Safari | Firefox |
| :-----:| :----: | :------:|
| &check;| x      |    x    |

## TODO
- Add division endpoint
- Upload to server
- Get feedback
